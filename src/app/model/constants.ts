export class Constants {
  static HEIGHT: number = 500;
  static WIDTH: number = 500;
  static ZOOM_STEP: number = 40;
  static TRANSLATION_STEP: number = 40;
  static ROTATION_STEP: number = 0.2;
}
