export enum TranslationType {
  LEFT,
  RIGHT,
  UP,
  DOWN,
  FORWARD,
  BACKWARD
}
