export class Line<POINT> {
  startPoint: POINT;
  endPoint: POINT;

  constructor(startPoint: POINT, endPoint: POINT) {
    this.startPoint = startPoint;
    this.endPoint = endPoint;
  }

}
