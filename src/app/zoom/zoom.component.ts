import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {ZoomType} from "../model/zoom-type.enum";

@Component({
  selector: 'gk-zoom',
  templateUrl: './zoom.component.html',
  styleUrls: ['./zoom.component.scss']
})
export class ZoomComponent implements OnInit {

  zoomType = ZoomType;

  @Output()
  zoom: EventEmitter<ZoomType> = new EventEmitter<ZoomType>();

  constructor() { }

  ngOnInit() {
  }

  zoomIt(zoomType: ZoomType): void {
    this.zoom.next(zoomType);
  }

}
