import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {TranslationType} from "../model/translation-type.enum";

@Component({
  selector: 'gk-translation',
  templateUrl: './translation.component.html',
  styleUrls: ['./translation.component.scss']
})
export class TranslationComponent implements OnInit {

  translationType = TranslationType;

  @Output()
  translation: EventEmitter<TranslationType> = new EventEmitter<TranslationType>();

  constructor() { }

  ngOnInit() {
  }

  translateIt(translationType: TranslationType): void {
    this.translation.next(translationType);
  }
}
