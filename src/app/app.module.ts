import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {AngularMaterialModule} from "./angular-material/angular-material.module";
import 'hammerjs';
import { FileReaderComponent } from './file-reader/file-reader.component';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import { CanvasComponent } from './canvas/canvas.component';
import { ZoomComponent } from './zoom/zoom.component';
import { TranslationComponent } from './translation/translation.component';
import { RotationComponent } from './rotation/rotation.component';

@NgModule({
  declarations: [
    AppComponent,
    FileReaderComponent,
    CanvasComponent,
    ZoomComponent,
    TranslationComponent,
    RotationComponent
  ],
  imports: [
    BrowserModule,
    AngularMaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
