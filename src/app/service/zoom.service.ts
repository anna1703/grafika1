import { Injectable } from '@angular/core';
import {Camera} from "../model/camera";
import {ZoomType} from "../model/zoom-type.enum";
import {Constants} from "../model/constants";

@Injectable({
  providedIn: 'root'
})
export class ZoomService {

  public static zoom(camera: Camera, zoomType: ZoomType): Camera {
    switch (zoomType) {
      case ZoomType.IN:
        return ZoomService.zoomIn(camera);
      case ZoomType.OUT:
        return ZoomService.zoomOut(camera);
    }
  }

  private static zoomIn(camera: Camera): Camera {
    camera.y += Constants.ZOOM_STEP;
    //noinspection TypeScriptUnresolvedFunction
    return Object.assign({}, camera);
  }

  private static zoomOut(camera: Camera): Camera {
    camera.y -= Constants.ZOOM_STEP;
    //noinspection TypeScriptUnresolvedFunction
    return Object.assign({}, camera);
  }

}
