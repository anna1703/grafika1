import { Injectable } from '@angular/core';
import {Point2D} from "../model/point2-d";
import {Point3D} from "../model/point3-d";
import {Line} from "../model/line";
import {Camera} from "../model/camera";

@Injectable({
  providedIn: 'root'
})
export class PerspectiveViewService {

  public static transformPoint(point: Point3D, camera: Camera): Point2D {
    let rate: number = camera.vpd / (point.y - camera.y);
    let x: number = rate * point.x;
    let y: number = rate * point.z;
    return new Point2D(x, y);
  }

  public static transformLine(line: Line<Point3D>, camera: Camera): Line<Point2D> {
    let startPoint: Point2D = this.transformPoint(line.startPoint, camera);
    let endPoint: Point2D = this.transformPoint(line.endPoint, camera);
    return new Line<Point2D>(startPoint, endPoint);
  }

  public static transformArrayOfLines(list: Array<Line<Point3D>>, camera: Camera): Array<Line<Point2D>> {
    let result: Array<Line<Point2D>> = [];
    list.forEach(element => result.push(this.transformLine(element, camera)));
    return result;
  }
}
