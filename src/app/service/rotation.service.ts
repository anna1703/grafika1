import { Injectable } from '@angular/core';
import {RotationType} from "../model/rotation-type.enum";
import {Point3D} from "../model/point3-d";
import {Line} from "../model/line";
import {Constants} from "../model/constants";

@Injectable({
  providedIn: 'root'
})
export class RotationService {

  public static rotate(data: Array<Line<Point3D>>, rotationType: RotationType): Array<Line<Point3D>> {
    switch (rotationType) {
      case RotationType.LEFT:
        return RotationService.rotateLeft(data);
      case RotationType.RIGHT:
        return RotationService.rotateRight(data);
      case RotationType.UP:
        return RotationService.rotateUp(data);
      case RotationType.DOWN:
        return RotationService.rotateDown(data);
      case RotationType.FORWARD:
        return RotationService.rotateForward(data);
      case RotationType.BACKWARD:
        return RotationService.rotateBackward(data);
    }
  }

  private static rotateLeft(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateLeftLine);
  }

  private static rotateLeftLine(line: Line<Point3D>): Line<Point3D> {
    let startX = (line.startPoint.x * Math.cos(-1 * Constants.ROTATION_STEP)) - (line.startPoint.y * Math.sin(-1 * Constants.ROTATION_STEP));
    let startY = (line.startPoint.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.startPoint.y * Math.cos(-1 * Constants.ROTATION_STEP));
    let endX = (line.endPoint.x * Math.cos(-1 * Constants.ROTATION_STEP)) - (line.endPoint.y * Math.sin(-1 * Constants.ROTATION_STEP));
    let endY = (line.endPoint.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.endPoint.y * Math.cos(-1 * Constants.ROTATION_STEP));
    let startPoint = new Point3D(startX, startY, line.startPoint.z);
    let endPoint = new Point3D(endX, endY, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static rotateRight(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateRightLine);
  }

  private static rotateRightLine(line: Line<Point3D>): Line<Point3D> {
    let startX = (line.startPoint.x * Math.cos(Constants.ROTATION_STEP)) - (line.startPoint.y * Math.sin(Constants.ROTATION_STEP));
    let startY = (line.startPoint.x * Math.sin(Constants.ROTATION_STEP)) + (line.startPoint.y * Math.cos(Constants.ROTATION_STEP));
    let endX = (line.endPoint.x * Math.cos(Constants.ROTATION_STEP)) - (line.endPoint.y * Math.sin(Constants.ROTATION_STEP));
    let endY = (line.endPoint.x * Math.sin(Constants.ROTATION_STEP)) + (line.endPoint.y * Math.cos(Constants.ROTATION_STEP));
    let startPoint = new Point3D(startX, startY, line.startPoint.z);
    let endPoint = new Point3D(endX, endY, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static rotateUp(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateUpLine);
  }

  private static rotateUpLine(line: Line<Point3D>): Line<Point3D> {
    let startX = (line.startPoint.x * Math.cos(-1 * Constants.ROTATION_STEP)) + (line.startPoint.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let startZ = (-1 * line.startPoint.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.startPoint.z * Math.cos(-1 * Constants.ROTATION_STEP));
    let endX = (line.endPoint.x * Math.cos(-1 * Constants.ROTATION_STEP)) + (line.endPoint.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let endZ = (-1 * line.endPoint.x * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.endPoint.z * Math.cos(-1 * Constants.ROTATION_STEP));
    let startPoint = new Point3D(startX, line.startPoint.y, startZ);
    let endPoint = new Point3D(endX, line.endPoint.y, endZ);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static rotateDown(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateDownLine);
  }

  private static rotateDownLine(line: Line<Point3D>): Line<Point3D> {
    let startX = (line.startPoint.x * Math.cos(Constants.ROTATION_STEP)) + (line.startPoint.z * Math.sin(Constants.ROTATION_STEP));
    let startZ = (-1 * line.startPoint.x * Math.sin(Constants.ROTATION_STEP)) + (line.startPoint.z * Math.cos(Constants.ROTATION_STEP));
    let endX = (line.endPoint.x * Math.cos(Constants.ROTATION_STEP)) + (line.endPoint.z * Math.sin(Constants.ROTATION_STEP));
    let endZ = (-1 * line.endPoint.x * Math.sin(Constants.ROTATION_STEP)) + (line.endPoint.z * Math.cos(Constants.ROTATION_STEP));
    let startPoint = new Point3D(startX, line.startPoint.y, startZ);
    let endPoint = new Point3D(endX, line.endPoint.y, endZ);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static rotateForward(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateForwardLine);
  }

  private static rotateForwardLine(line: Line<Point3D>): Line<Point3D> {
    let startY = (line.startPoint.y * Math.cos(-1 * Constants.ROTATION_STEP)) - (line.startPoint.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let startZ = (line.startPoint.y * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.startPoint.z * Math.cos(-1 * Constants.ROTATION_STEP));
    let endY = (line.endPoint.y * Math.cos(-1 * Constants.ROTATION_STEP)) - (line.endPoint.z * Math.sin(-1 * Constants.ROTATION_STEP));
    let endZ = (line.endPoint.y * Math.sin(-1 * Constants.ROTATION_STEP)) + (line.endPoint.z * Math.cos(-1 * Constants.ROTATION_STEP));
    let startPoint = new Point3D(line.startPoint.x, startY, startZ);
    let endPoint = new Point3D(line.endPoint.x, endY, endZ);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static rotateBackward(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(RotationService.rotateBackwardLine);
  }

  private static rotateBackwardLine(line: Line<Point3D>): Line<Point3D> {
    let startY = (line.startPoint.y * Math.cos(Constants.ROTATION_STEP)) - (line.startPoint.z * Math.sin(Constants.ROTATION_STEP));
    let startZ = (line.startPoint.y * Math.sin(Constants.ROTATION_STEP)) + (line.startPoint.z * Math.cos(Constants.ROTATION_STEP));
    let endY = (line.endPoint.y * Math.cos(Constants.ROTATION_STEP)) - (line.endPoint.z * Math.sin(Constants.ROTATION_STEP));
    let endZ = (line.endPoint.y * Math.sin(Constants.ROTATION_STEP)) + (line.endPoint.z * Math.cos(Constants.ROTATION_STEP));
    let startPoint = new Point3D(line.startPoint.x, startY, startZ);
    let endPoint = new Point3D(line.endPoint.x, endY, endZ);
    return new Line<Point3D>(startPoint, endPoint);
  }
}

