import { Injectable } from '@angular/core';
import {Line} from "../model/line";
import {Point3D} from "../model/point3-d";

@Injectable({
  providedIn: 'root'
})
export class FileReaderService {

  public static readFile(fileContent: String): Array<Line<Point3D>> {
    let result = [];
    let lines = fileContent.split('\n');
    for (let i = 0; i < lines.length; i++) {
      let line = lines[i].trim().replace(new RegExp(";", 'g'), '');
      if (line.charAt(0) !== '#') {
        let coordinates = line.split(" ");
        result.push(new Line<Point3D>(new Point3D(+coordinates[0], +coordinates[1], +coordinates[2]),
                             new Point3D(+coordinates[3], +coordinates[4], +coordinates[5])));
      }
    }
    return result;
  }

}
