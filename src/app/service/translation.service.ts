import { Injectable } from '@angular/core';
import {Line} from "../model/line";
import {Point3D} from "../model/point3-d";
import {Constants} from "../model/constants";
import {TranslationType} from "../model/translation-type.enum";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  public static translate(data: Array<Line<Point3D>>, translationType: TranslationType): Array<Line<Point3D>> {
    switch (translationType) {
      case TranslationType.FORWARD:
        return TranslationService.translateForward(data);
      case TranslationType.BACKWARD:
        return TranslationService.translateBackward(data);
      case TranslationType.LEFT:
        return TranslationService.translateLeft(data);
      case TranslationType.RIGHT:
        return TranslationService.translateRight(data);
      case TranslationType.DOWN:
        return TranslationService.translateDown(data);
      case TranslationType.UP:
        return TranslationService.translateUp(data);
    }
  }

  private static translateForward(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateForwardLine);
  }

  private static translateForwardLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x, line.startPoint.y - Constants.TRANSLATION_STEP, line.startPoint.z);
    let endPoint = new Point3D(line.endPoint.x, line.endPoint.y - Constants.TRANSLATION_STEP, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static translateBackward(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateBackwardLine);
  }

  private static translateBackwardLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x, line.startPoint.y + Constants.TRANSLATION_STEP, line.startPoint.z);
    let endPoint = new Point3D(line.endPoint.x, line.endPoint.y + Constants.TRANSLATION_STEP, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static translateLeft(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateLeftLine);
  }

  private static translateLeftLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x + Constants.TRANSLATION_STEP, line.startPoint.y, line.startPoint.z);
    let endPoint = new Point3D(line.endPoint.x + Constants.TRANSLATION_STEP, line.endPoint.y, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static translateRight(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateRightLine);
  }

  private static translateRightLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x - Constants.TRANSLATION_STEP, line.startPoint.y, line.startPoint.z);
    let endPoint = new Point3D(line.endPoint.x - Constants.TRANSLATION_STEP, line.endPoint.y, line.endPoint.z);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static translateUp(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateUpLine);
  }

  private static translateUpLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x, line.startPoint.y, line.startPoint.z - Constants.TRANSLATION_STEP);
    let endPoint = new Point3D(line.endPoint.x, line.endPoint.y, line.endPoint.z - Constants.TRANSLATION_STEP);
    return new Line<Point3D>(startPoint, endPoint);
  }

  private static translateDown(data: Array<Line<Point3D>>): Array<Line<Point3D>> {
    return data.map(TranslationService.translateDownLine);
  }

  private static translateDownLine(line: Line<Point3D>): Line<Point3D> {
    let startPoint = new Point3D(line.startPoint.x, line.startPoint.y, line.startPoint.z + Constants.TRANSLATION_STEP);
    let endPoint = new Point3D(line.endPoint.x, line.endPoint.y, line.endPoint.z + Constants.TRANSLATION_STEP);
    return new Line<Point3D>(startPoint, endPoint);
  }
}
