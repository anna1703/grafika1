import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {RotationType} from "../model/rotation-type.enum";

@Component({
  selector: 'gk-rotation',
  templateUrl: './rotation.component.html',
  styleUrls: ['./rotation.component.scss']
})
export class RotationComponent implements OnInit {

  rotationType = RotationType;

  @Output()
  rotation: EventEmitter<RotationType> = new EventEmitter<RotationType>();

  constructor() { }

  ngOnInit() {
  }

  rotateIt(rotationType: RotationType): void {
    this.rotation.next(rotationType);
  }
}
