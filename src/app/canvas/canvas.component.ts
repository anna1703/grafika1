import {Component, OnInit, ViewChild, ElementRef, Input, OnChanges, SimpleChanges} from '@angular/core';
import {Point3D} from "../model/point3-d";
import {Line} from "../model/line";
import {Point2D} from "../model/point2-d";
import {PerspectiveViewService} from "../service/perspective-view.service";
import {Camera} from "../model/camera";
import {Constants} from "../model/constants";

@Component({
  selector: 'gk-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss']
})
export class CanvasComponent implements OnInit, OnChanges {

  @ViewChild('canvas')
  canvasRef: ElementRef;

  @Input()
  data: Array<Line<Point3D>>;

  @Input()
  camera: Camera;

  data2d: Array<Line<Point2D>>;

  canvas: any;
  ctx: any;

  constructor() { }

  ngOnInit() {
    this.canvas = this.canvasRef['nativeElement'];
    this.ctx = this.canvas.getContext('2d');

    // this.canvas.style.width  = '900px';
    // this.canvas.style.height = '450px';

    this.canvas.width = Constants.WIDTH;
    this.canvas.height = Constants.HEIGHT;

    let transX: number = this.canvas.width * 0.5;
    let transY: number = this.canvas.height * 0.5;
    this.ctx.translate(transX, transY);
    this.ctx.scale(1, -1);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if ((changes['data'] && changes['data'].currentValue) || (changes['camera'] && changes['camera'].currentValue)
        && this.data && this.camera) {
      this.data2d = PerspectiveViewService.transformArrayOfLines(this.data, this.camera);
      this.clear();
      this.print();
    }
  }

  print(): void {
    this.data2d.forEach(line => {
      this.ctx.beginPath();
      this.ctx.moveTo(line.startPoint.x, line.startPoint.y);
      this.ctx.lineTo(line.endPoint.x, line.endPoint.y);
      this.ctx.closePath();
      this.ctx.stroke();
    });
  }

  clear(): void {
    // Store the current transformation matrix
    this.ctx.save();
    // Use the identity matrix while clearing the canvas
    this.ctx.setTransform(1, 0, 0, 1, 0, 0);
    this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
    // Restore the transform
    this.ctx.restore();
  }
}
