import {Component, OnInit, Output, EventEmitter} from '@angular/core';
import {FormBuilder, FormGroup} from "@angular/forms";
import {Line} from "../model/line";
import {FileReaderService} from "../service/file-reader.service";
import {Point3D} from "../model/point3-d";

@Component({
  selector: 'gk-file-reader',
  templateUrl: './file-reader.component.html',
  styleUrls: ['./file-reader.component.scss']
})
export class FileReaderComponent implements OnInit {

  form: FormGroup;
  fileContent: any;
  fileName: String;

  @Output()
  dataChange: EventEmitter<Array<Line<Point3D>>> = new EventEmitter<Array<Line<Point3D>>>();

  constructor(private fb: FormBuilder) {
    this.form = this.fb.group({
      'file': ['']
    });
  }

  ngOnInit() {
  }

  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsText(file);
      reader.onload = () => {
        this.fileContent = reader.result;
        this.fileName = file.name;
      }
    }
  }

  readFile() {
    this.dataChange.next(FileReaderService.readFile(this.fileContent));
  }
}
