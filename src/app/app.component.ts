import { Component } from '@angular/core';
import {Point3D} from "./model/point3-d";
import {Line} from "./model/line";
import {Camera} from "./model/camera";
import {ZoomType} from "./model/zoom-type.enum";
import {TranslationType} from "./model/translation-type.enum";
import {RotationType} from "./model/rotation-type.enum";
import {TranslationService} from "./service/translation.service";
import {ZoomService} from "./service/zoom.service";
import {RotationService} from "./service/rotation.service";

@Component({
  selector: 'gk-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  data: Array<Line<Point3D>>;
  camera: Camera;

  loadData(event): void {
    this.data = event;
    this.camera = new Camera(350, 0);
  }

  doZoom(event: ZoomType): void {
    this.camera = ZoomService.zoom(this.camera, event);
  }

  doTranslation(event: TranslationType): void {
    this.data = TranslationService.translate(this.data, event);
  }

  doRotation(event: RotationType): void {
    this.data = RotationService.rotate(this.data, event);
  }

}
